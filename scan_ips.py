import os
from shodan import Shodan
from pprint import pprint
import json

def usage():
    print("\t   _____   _____   ______   ______   _____ ")
    print("\t  / ____| |_   _| |  ____| |___  /  / ____|")
    print("\t | |  __    | |   | |__       / /  | (___  ")
    print("\t | | |_ |   | |   |  __|     / /    \___ \ ")
    print("\t | |__| |  _| |_  | |       / /__   ____) |")
    print("\t  \_____| |_____| |_|      /_____| |_____/ ")
    print("\t    _________  ________  ________  ___          ")
    print("\t   |\___   ___\\   __   \|\   __  \|\  \           ")
    print("\t   \|___ \  \_\ \  \|\  \ \  \|\  \ \  \        ")
    print("\t        \ \  \ \ \  \|\  \ \  \|\  \ \  \       ")
    print("\t         \ \  \ \ \  \|\  \ \  \|\  \ \  \____  ")
    print("\t          \ \__\ \ \_______\ \_______\ \_______\ ")
    print("\t           \|__|  \|_______|\|_______|\|_______| ")
    print("\n")
    print("\t\tGet Information From Zmap & Sh****")

def scan_shodan(output_file, shodan_key_file, search_string):
    shodankey = open(shodan_key_file).readline().rstrip('\n')
    api = Shodan(shodankey)
    still_results = True
    page = 1

    results = api.search(search_string, page)
    still_results = bool(results)

    ips = []
    try:
        while  still_results:
            for ip in results['matches']:
                tmp_ip = {}
                tmp_ip['ip'] = ip['ip_str']
                tmp_ip['port'] = ip['port']
                ips.append(tmp_ip)

                pprint(tmp_ip)
            page += 1
            results = api.search('raspbian', page)
            still_results = bool(results)
    except Exception as e:
        print e
    finally:
        with open(output_file, 'w') as outfile:
            json.dump(ips, outfile)

def scan_zmap(output_file, port, resul):
    cmd = 'sudo zmap -N '+ resul +' -B 20M -p' + port + ' -o ' + output_file
    os.system(cmd)

def main():

    usage()
    print("Name of output file: ej: data01.txt")
    output_file = raw_input("Name File: ")
    print("Pick wich scanner: 1 for Zmap 2 for Shodan")
    scanner_option = int(raw_input("Select Scanner: "))

    use_zmap = (scanner_option == 1)
    if use_zmap:
        print("Select Port to Scan: ej: 80")
        port = str(raw_input("Port: "))
        print("Select number of results: Ej: 100")
        resul = str(raw_input("Number of results: "))
        scan_zmap(output_file=output_file, port=port, resul=resul)
    else:
        shodan_key_file = raw_input("Shodan key file route: ")
        search_string = raw_input("Search string for shodan: ")
        scan_shodan(output_file=output_file, shodan_key_file=shodan_key_file, search_string=search_string)

    print

main()
